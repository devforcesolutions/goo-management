1. Se si usa intelliJ, copiare la cartella runConfigurations dentro la cartella .idea
2. Installare PostgreSQL 9.4 e scegliere username "postgres" e password "postgres"
3. Avviare un terminale e digitare "psql -U postgres"
4. Lanciare il comando "create database goo_management;"
5. Chiudere il terminale ad operazione conclusa
6. Fare una copia del file local.properies.sample nella stessa directory
7. Rinominare il file creato in "local.properties" (rimuovere .sample)
8. Estrarre Wildfly dall'archivio in una cartella a piacere (si trova solo nel ramo master)
9. Sostituire la proprietà output.directory con il percorso giusto di wildfly estratto al punto 8
10. Cambiare ramo (in basso a destra cliccare su Git: master -> remote branches -> first-dev (o training-marco) -> checkout As -> OK) e verificare che sia apparsa la cartella src
11. Lanciare DB-Reset (se è stato fatto il punto 1 basta selezionarlo in alto a destra e cliccare play, altrimenti lanciare mvn flyway:clean e successivamente mvn flyway:migrate)
12. Ad operazione conclusa, lanciare il comando deploy (selezionarlo dal menu in alto a destra con IntelliJ o lanciare mvn clean install)
13. Se tutto è andato a buon fine, dentro l'output.directory definita al punto 9 ci sarà un file chiamato GOO.war
14. Andare in {wildfly-home}\bin e lanciare standalone.bat
15. Installare ed avviare SoapUI
16. file->import project e selezionare il file SOAPUI.xml presente nella root del progetto
17. SoapUI importerà tutti i metodi REST per testare l'applicazione
